package com.vivandro.androiddialogtutorial;

import com.vivandro.androiddialogtutorial.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;


public class SimpleAlertDialog extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
     // 1. Instantiate an AlertDialog.Builder with its constructor
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // 2. Chain together various setter methods to set the dialog characteristics
        builder.setMessage(R.string.dialog_message)
               .setTitle(R.string.dialog_title);

        // 3. Get the AlertDialog from create()
        AlertDialog dialog = builder.create();
        return dialog;        
    }
    
}

